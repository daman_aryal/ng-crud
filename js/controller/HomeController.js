var app = angular.module("myModule", ['ngRoute']);

app.factory("CrudFactory", function(){
    var services = {};
    services.students = [
        {
            "id":"1",
            "name":"Daman",
            "address":"Samakhusi"
        },
        {
            "id":"2",
            "name":"Prajwal",
            "address":"Kapan"
        }
    ];

   services.addRecord = function(id, name, address){
       /*console.log(services.students[0]);*/
        services.students.push({
            "id" : id,
            "name":name,
            "address":address
        });
    };

    return services;


});

app.controller("myController", function($scope, CrudFactory, $routeParams){

    $scope.datas = CrudFactory.students;

    $scope.add = function(){
        CrudFactory.addRecord($scope.id, $scope.name, $scope.address);
    };



    /*delete*/


/*
    CrudFactory.addRecord($scope.id, $scope.name, $scope.address);
*/
    /* $scope.addStudent = function(){
        $scope.students.push({
            "id" : $scope.id,
            "name" : $scope.name,
            "address" : $scope.address
        });
    }*/
});

/*edit controller*/
            app.controller("editController", function($scope, $routeParams, CrudFactory){
                for(var i=0; i < CrudFactory.students.length; i++){
                    if(CrudFactory.students[i].id == $routeParams.id){
                        $scope.editId = CrudFactory.students[i].id;
                        $scope.editName = CrudFactory.students[i].name;
                        $scope.editAddress = CrudFactory.students[i].address;

                        $scope.updateRecord = function(){
                            var id = $routeParams.id - 1;

                            CrudFactory.students[id] = {
                                id : $scope.editId,
                                name : $scope.editName,
                                address : $scope.editAddress
                            };

                            /*CrudFactory.students[id].splice(CrudFactory.students[id].id, 0, "daman");*/


                        };
                    }
                }
            });



            app.controller("deleteController", function($scope, CrudFactory, $routeParams){
                        for(var i=0; i < CrudFactory.students.length; i++){

                            if(CrudFactory.students[i].id == $routeParams.id){
                                $scope.editId = CrudFactory.students[i].id;
                                $scope.editName = CrudFactory.students[i].name;
                                $scope.editAddress = CrudFactory.students[i].address;

                                var id = $routeParams.id - 1;
                                CrudFactory.students.splice(CrudFactory.students[id],1);

                            }
                        }


            });
/*End of edit controller*/


app.config([
    "$routeProvider", function($routeProvider){
        $routeProvider.when("/home", {
            templateUrl : "view/home.html",
            controller : "myController"
        }).when(
            "/edit/:id",{
                templateUrl : "view/edit.html",
                controller : "editController"
            }
        ).when(
            "/delete/:id",{
                templateUrl : "view/delete.html",
                controller :"deleteController"
            }
        )
    }
]);